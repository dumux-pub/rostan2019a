add_input_file_links()


dune_add_test(NAME grid2msh_
              SOURCES grid2msh.cc
              LABELS unit io
              CMAKE_GUARD dune-subgrid_FOUND
              COMMAND ${CMAKE_SOURCE_DIR}/bin/testing/runtest.py
              CMD_ARGS --script fuzzy
                       --command "${CMAKE_CURRENT_BINARY_DIR}/grid2msh"
                       --files ${CMAKE_SOURCE_DIR}/test/references/subgrid-reference.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/subgrid_circle_yasp.vtu
                               ${CMAKE_SOURCE_DIR}/test/references/test_gridmanager_subgrid_binary_image.vtu
                               ${CMAKE_CURRENT_BINARY_DIR}/subgrid_binary_image.vtu)

