/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the cake grid manager
 */
#include <config.h>
#include <iostream>
#include <cmath>
#include <string>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/gmshwriter.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/common/parameters.hh>


int main(int argc, char** argv) try
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);

    constexpr int dim = 3;

    {
        Dune::Timer timer;
        std::cout << "Constructing SubGrid from raw" << std::endl;

        using Grid = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<double, dim> >;  
        using GridManager_ = Dumux::GridManager<Grid>;  

        GridManager_ gridManager;
         
        gridManager.init();

        Dune::VTKWriter<typename  Grid::LeafGridView> vtkWriter(gridManager.grid().leafGridView());
        vtkWriter.write("test");

        Dune::GmshWriter<typename  Grid::LeafGridView> gmshWriter(gridManager.grid().leafGridView());
        gmshWriter.write("test.msh");

        std::cout << "Constructing took "  << timer.elapsed() << " seconds.\n";
   
    
    }

    return 0;
}
///////////////////////////////////////
//////// Error handler ////////////////
///////////////////////////////////////
catch (const Dumux::ParameterException& e) {
    std::cerr << e << ". Abort!\n";
    return 1;
}
catch (const Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 3;
}
catch (...) {
    std::cerr << "Unknown exception thrown!\n";
    return 4;
}
