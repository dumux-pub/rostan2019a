# import necessary packages
import openpnm as op
from pathlib import Path
import numpy as np

path = Path('Berea/');
filename_output = 'berea_ms';


#calculation of L and A for a cube
voxel_size = 3e-6;
voxel_number = 400;
L = voxel_size * voxel_number;
A = L*L;
#open project
project = op.io.Statoil.load(path=path, prefix='berea');
pn = project.network;
#trimming pore network to avoid singularity
print('Number of pores before trimming: ', pn.Np);
h = pn.check_network_health();
op.topotools.trim(network=pn, pores=h['trim_pores']);
print('Number of pores after trimming: ', pn.Np);
#add pore diameter and throat diameter
pn['pore.diameter'] = pn['pore.radius']*2;
pn['throat.diameter'] = pn['throat.radius']*2;


throats= np.asarray(pn['throat.conns'])
poreAll = np.asarray(pn['pore.all'])
poreVolume = np.asarray(pn['pore.volume'])
throatVolume = np.asarray(pn['throat.volume'])
numberPore = poreAll.size



# assign throats connected to pores
throatsPerPore = [[] for _ in range(numberPore)]

for throatIdx, pores in enumerate(throats):
    pore0 = pores[0]
    pore1 = pores[1]

    throatsPerPore[pore0].append(throatIdx)
    throatsPerPore[pore1].append(throatIdx)

throatsPerPore = np.asarray(throatsPerPore)


for i in range(numberPore):
    poreVolume_ = 0
    throatsPerPore_ = np.asarray(throatsPerPore[i], dtype=int)
    throatVolume_ = throatVolume[throatsPerPore[i]]
    throatsPerPore_size = throatsPerPore_.size
    poreVolume_ = poreVolume[i]

    for j in range(throatsPerPore_size):
        poreVolume_ += throatVolume_[j]/2
        

    poreVolume[i] =  poreVolume_
        
pn['pore.volume'] = poreVolume

#export to vtk
op.io.VTK.save(network=pn, filename=filename_output);
