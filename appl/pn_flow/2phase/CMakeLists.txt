add_input_file_links("snow.input" "ms.input")

dune_symlink_to_source_files(FILES grids)

dune_add_test(NAME pn_2p
              SOURCES pn_2p.cc
              LABELS porenetworkflow
              COMMAND ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
              CMD_ARGS       --script fuzzyData
                             --files ${CMAKE_SOURCE_DIR}/test/references/test_pnm_2p_static-reference.txt
                                     ${CMAKE_CURRENT_BINARY_DIR}/test_pnm_2p_static_pc-s-curve.txt
                             --command "${CMAKE_CURRENT_BINARY_DIR}/test_pnm_2p_static")
