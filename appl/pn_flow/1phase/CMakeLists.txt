dune_symlink_to_source_files(FILES EPS IPS EPS-G EPS-VB)

dune_add_test(NAME pn_eps_1p
              COMPILE_DEFINITIONS TRANSMISSIBILITY=TransmissibilityEPS<Scalar>
              SOURCES main.cc)

dune_add_test(NAME pn_eps-g_1p
              COMPILE_DEFINITIONS TRANSMISSIBILITY=TransmissibilityGostick<Scalar>
              SOURCES main.cc)

dune_add_test(NAME pn_eps-vb_1p
              COMPILE_DEFINITIONS TRANSMISSIBILITY=TransmissibilityValvatneBlunt<Scalar>
              SOURCES main.cc)

dune_add_test(NAME pn_ips_1p
              COMPILE_DEFINITIONS TRANSMISSIBILITY=TransmissibilityIPS<Scalar>
              SOURCES main.cc)
