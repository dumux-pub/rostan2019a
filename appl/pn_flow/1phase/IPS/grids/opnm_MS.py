#openpnm permeability_statoil

import openpnm as op
import scipy as sp
from pathlib import Path


path = Path('Berea/')
#filename_output = 'Berea_Statoil_x'

#calculation of L and A for a cube
voxel_size = 3e-6;
voxel_number = 400;
L = voxel_size * voxel_number;
A = L*L;


project = op.io.Statoil.load(path=path, prefix='berea')

pn = project.network

print(pn)

print('Number of pores before trimming: ', pn.Np)
h = pn.check_network_health()
op.topotools.trim(network=pn, pores=h['trim_pores'])
print('Number of pores after trimming: ', pn.Np)

geo = op.geometry.GenericGeometry(network=pn, pores=pn.Ps, throats=pn.Ts)

#add pore diameter and throat diameter
pn['pore.diameter'] = pn['pore.radius']*2;
pn['throat.diameter'] = pn['throat.radius']*2;

# add water phase
water = op.phases.Water(network=pn);

## viscosity
mu_w = sp.mean(water['pore.viscosity'])

# calculate conductance
water.add_model(propname='throat.hydraulic_conductance',
                model=op.models.physics.hydraulic_conductance.classic_hagen_poiseuille)

# Specify a pressure difference (in Pa)
delta_P = 0.94

flow = op.algorithms.StokesFlow(network=pn, phase=water)
flow.set_value_BC(pores=pn.pores('inlets'), values=1000.94)
flow.set_value_BC(pores=pn.pores('outlets'), values=1000)
flow.run()
# Using the rate method of the StokesFlow algorithm
Q = sp.absolute(flow.rate(pores=pn.pores('outlets')))
# Because we know the inlets and outlets are at x=0 and x=X
#Lx = sp.amax(pn['pore.coords'][:, 0]) - sp.amin(pn['pore.coords'][:, 0])
#A = Lx*Lx  # Since the network is cubic Lx = Ly = Lz
K = Q*mu_w*L/(delta_P*A)
print('K(m²)=',K)
print('K(mD)=',K/9.869233e-16)

water.update(flow.results())

pn['pore.diameter'] = pn['pore.radius']*2
pn['throat.diameter'] = pn['throat.radius']*2


#project.export_data(filename=filename_output, filetype='vtk')
#op.io.VTK.save(network=pn, phases=water, filename=filename_output)