#! /usr/bin/env python

"""
Convert a vtp file to a dgf file
Script for specific openpnm vtp file to dgf readable by the dumux porenetwork module
"""
import argparse
import xml.etree.ElementTree as ET
import sys
import os
import numpy as np
import re

if __name__ == "__main__":
    # read the command line arguments
    parser = argparse.ArgumentParser(description='vtp to dgf converter')
    parser.add_argument('vtpfile', type=str, help='The vtp file to convert')
    args = vars(parser.parse_args())

    # construct element tree from vtk file
    tree = ET.parse(args["vtpfile"])
    root = tree.getroot()
    piece = root[0][0]

    vertices = np.fromstring(piece.find("Points/DataArray[@Name='coords']").text, sep=" ", dtype=np.float64)
    vertices = vertices.reshape(len(vertices)//3, 3)
    connectivity = np.fromstring(piece.find("Lines/DataArray[@Name='connectivity']").text, sep=" ", dtype=np.int64)
    elements = connectivity.reshape(len(connectivity)//2, 2)

    # get point data (pore radius, pore label)
    for dataArray in root.findall(".//PointData/DataArray"):
        if "pore.diameter" in dataArray.attrib["Name"]:
            poreRadius = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)*0.5
        if "pore.area" in dataArray.attrib["Name"]:
            poreArea = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)
        if "pore.inlets" in dataArray.attrib["Name"]:
            poreInlets = np.fromstring(dataArray.text, sep=" ", dtype=np.int64)
        if "pore.outlets" in dataArray.attrib["Name"]:
            poreOutlets = np.fromstring(dataArray.text, sep=" ", dtype=np.int64)

    # get cell data (throat radius, throat length, throat label)
    for dataArray in root.findall(".//CellData/DataArray"):
        if "throat.diameter" in dataArray.attrib["Name"]:
            throatRadius = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)*0.5
        if "throat.area" in dataArray.attrib["Name"]:
            throatArea = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)
        if "throat.conduit_lengths.throat" in dataArray.attrib["Name"]:
            throatLength = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)
        if "throat.conduit_lengths.pore1" in dataArray.attrib["Name"]:
            throatConduitLengthPore0 = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)
        if "throat.conduit_lengths.pore2" in dataArray.attrib["Name"]:
            throatConduitLengthPore1 = np.fromstring(dataArray.text, sep=" ", dtype=np.float64)

    
    # create boundary pore and throat labels
    poreLabel = np.zeros(len(poreRadius), dtype=int)
    throatLabel = np.zeros(len(poreRadius), dtype=int)

    bBox = np.array([[np.min(vertices[:,0]), np.min(vertices[:,1]), np.min(vertices[:,2])],
                     [np.max(vertices[:,0]), np.max(vertices[:,1]), np.max(vertices[:,2])]])
      
    poreInlets = np.array(poreInlets, dtype=bool)
    poreOutlets = np.array(poreOutlets, dtype=bool)     


    inflowVertices = np.where(poreInlets == True)

    outflowVertices = np.where(poreOutlets == True)



    poreLabel = np.full(len(poreRadius), -1, dtype=int)
    throatLabel = np.full(len(throatRadius), -1, dtype=int)

    # set pore labels for inflow and outflow
    poreLabel[inflowVertices] = 2
    poreLabel[outflowVertices] = 3

    # compute throat labels
    for i in range(len(elements)):
        if elements[i,0] in inflowVertices[0] or elements[i,1] in inflowVertices[0]:
            throatLabel[i] = 2
        if elements[i,0] in outflowVertices[0] or elements[i,1] in outflowVertices[0]:
            throatLabel[i] = 3

    # create dgf data
    vdata = np.stack([vertices[:,0], vertices[:,1], vertices[:,2], poreRadius, poreLabel, poreArea], axis=1).reshape(len(poreLabel), 6)
    edata = np.stack([elements[:,0], elements[:,1], throatRadius, throatLength, throatLabel, throatArea, throatConduitLengthPore0,  throatConduitLengthPore1], axis=1).reshape(len(throatLabel), 8)

    # write DGF file
    with open(os.path.splitext(args["vtpfile"])[0] + ".dgf", "w") as outputfile:
        outputfile.write("DGF\n")
        outputfile.write("Vertex\n")
        outputfile.write("parameters 3\n") # vertex data
        for i in range(len(poreLabel)):
            outputfile.write("{} {} {} {} {} {}\n".format(vertices[i,0], vertices[i,1], vertices[i,2], poreRadius[i], poreLabel[i], poreArea[i]))
        outputfile.write("\n#\n")
        outputfile.write("SIMPLEX\n")
        outputfile.write("parameters 6\n") # cell data
        for i in range(len(throatLabel)):
            outputfile.write("{} {} {} {} {} {} {} {}\n".format(elements[i,0], elements[i,1], throatRadius[i], throatLength[i], throatLabel[i], throatArea[i], throatConduitLengthPore0[i], throatConduitLengthPore1[i]))
        outputfile.write("\n#\n")
        outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")

