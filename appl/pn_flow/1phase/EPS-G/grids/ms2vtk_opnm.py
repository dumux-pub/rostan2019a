# import necessary packages
import openpnm as op
from pathlib import Path

path = Path('Berea/');
filename_output = 'berea_ms';


#calculation of L and A for a cube
voxel_size = 3e-6;
voxel_number = 400;
L = voxel_size * voxel_number;
A = L*L;
#open project
project = op.io.Statoil.load(path=path, prefix='berea');
pn = project.network;
#trimming pore network to avoid singularity
print('Number of pores before trimming: ', pn.Np);
h = pn.check_network_health();
op.topotools.trim(network=pn, pores=h['trim_pores']);
print('Number of pores after trimming: ', pn.Np);
#add pore diameter and throat diameter
pn['pore.diameter'] = pn['pore.radius']*2;
pn['throat.diameter'] = pn['throat.radius']*2;
# add water phase
water = op.phases.Water(network=pn);
# add shape factors
water.add_model(propname='throat.flow_shape_factors',
               model=op.models.physics.flow_shape_factors.ball_and_stick)
pn['throat.poreshape_factor1'] = water['throat.flow_shape_factors.pore1'];
pn['throat.poreshape_factor2'] = water['throat.flow_shape_factors.pore2'];
pn['throat.shape_factor'] = water['throat.flow_shape_factors.throat'];
#export to vtk
op.io.VTK.save(network=pn, filename=filename_output);
