#SNOW algorithm

# import necessary packages
import porespy as ps
import openpnm as op
import nrrd
import scipy as sp

filename_input = 'Berea.nrrd';
filename_output = 'berea_snow';

# create workspace instance
ws = op.Workspace();    
# clear workspace    
ws.clear();                                  
print(ws.keys()); 
# create project
project = ws.new_project(name=filename_output);
# read inputfile
im, header = nrrd.read(filename_input);
# convert im to bool and invert it
im = sp.array(im, dtype=bool);
im = ~im;
# print porosity
porosity = ps.metrics.porosity(im);
print(porosity);
#application of snow-algorithm
snow_output = ps.networks.snow(im,
                  voxel_size=3e-6,
                  boundary_faces=['top', 'bottom', 'left', 'right', 'front', 'back'],
                  marching_cubes_area=False)
# create empty dummy network
pn = op.network.GenericNetwork(project=project);
# update openpnm pore network
pn.update(snow_output);
# trimming pore network to avoid singularity
print('Number of pores before trimming: ', pn.Np);
h = pn.check_network_health();
op.topotools.trim(network=pn, pores=h['trim_pores']);
print('Number of pores after trimming: ', pn.Np);
# add water phase
water = op.phases.Water(network=pn);
# add shape factors
water.add_model(propname='throat.flow_shape_factors',
               model=op.models.physics.flow_shape_factors.ball_and_stick)
pn['throat.poreshape_factor1'] = water['throat.flow_shape_factors.pore1'];
pn['throat.poreshape_factor2'] = water['throat.flow_shape_factors.pore2'];
pn['throat.shape_factor'] = water['throat.flow_shape_factors.throat'];
# check workspace
print(ws.keys()); 
# export to vtk
op.io.VTK.save(network=pn, filename=filename_output)
#op.io.VTK.save(network=pn, phases=water, filename=filename_output)
# save project
ws.save_project(project, filename=filename_output)
# close project)
ws.close_project(project)


