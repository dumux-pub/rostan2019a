// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A upscaling problem for the one-phase pore network model.
 */
#ifndef DUMUX_UPSCALING1P_PROBLEM_HH
#define DUMUX_UPSCALING1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1p/model_rostan2019a.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include "spatialparams.hh"

namespace Dumux {

template <class TypeTag>
class UpscalingProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct UpscalingProblemTypeTag { using InheritsFrom = std::tuple<PNMOneP>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::UpscalingProblemTypeTag> { using type = Dumux::UpscalingProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::UpscalingProblemTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::SimpleH2O<Scalar> >;
};

template<class TypeTag>
struct SinglePhaseTransmissibilityLaw<TypeTag, TTag::UpscalingProblemTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = TRANSMISSIBILITY;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::UpscalingProblemTypeTag> { using type = Dune::FoamGrid<1, 3>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::UpscalingProblemTypeTag>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Transmissibility = GetPropType<TypeTag, Properties::SinglePhaseTransmissibilityLaw>;
public:
    using type = PNMOnePUpscalingSpatialParams<GridGeometry, Transmissibility, Scalar>;
};

}

template <class TypeTag>
class UpscalingProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    struct UpscaledData
    {
        Scalar outflowArea;
        Scalar massFlux;
        Scalar vDarcy;
        Scalar K;
    };

public:
    template<class SpatialParams>
    UpscalingProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
        : ParentType(gridGeometry, spatialParams), eps_(1e-7), dirNames_({{"x", "y", "z"}})
    {
        // get some parameters from the input file
        direction_ = 0;
        pressureGradient_ = getParam<Scalar>("Problem.PressureGradient");
        isTest_ = getParam<bool>("Problem.IsTest", false);

        computeExtendedBoundingBox();

        auto sideLength = [&](int i)
        {
            static const bool useExtendedBoundingBox = getParam<bool>("Problem.UseExtendedBoundingBox", true);
            if (useExtendedBoundingBox)
                return extendedBBoxMax()[i] - extendedBBoxMin()[i];
            else
                return this->gridGeometry().bBoxMax()[i] - this->gridGeometry().bBoxMin()[i];
        };

        // get the domain's dimensions
        for (int i = 0; i < length_.size(); ++i)
        {
            std::cout << dirNames_[i] << ": " << "standard bBoxMin " << this->gridGeometry().bBoxMin()[i] << ", extended " << extendedBBoxMin()[i] << std::endl;
            std::cout << dirNames_[i] << ": " << "standard bBoxMax " << this->gridGeometry().bBoxMax()[i] << ", extended " << extendedBBoxMax()[i] << std::endl;
            length_[i] = sideLength(i);
        }

        this->spatialParams().updateBoundaryFactor(direction_);

    }

    void computeExtendedBoundingBox()
    {
        // calculate the bounding box of the local partition of the grid view
        for (const auto& vertex : vertices(this->gridGeometry().gridView()))
        {
            const auto vIdx = this->gridGeometry().vertexMapper().index(vertex);
            const Scalar poreRadius = this->gridGeometry().poreRadius(vIdx);

            for (int i = 0; i < dimworld; i++)
            {
                using std::min;
                using std::max;

                const Scalar additionalDistance = (this->gridGeometry().poreGeometry(vIdx) == Pore::Shape::cylinder) && i == dimworld-1
                                                ? poreRadius*this->gridGeometry().poreAspectRatio(vIdx) /*==0.5*height*/
                                                : poreRadius;


                bBoxMin_[i] = min(bBoxMin_[i], vertex.geometry().corner(0)[i] - additionalDistance);
                bBoxMax_[i] = max(bBoxMax_[i], vertex.geometry().corner(0)[i] + additionalDistance);
            }
        }
    }

    const auto& extendedBBoxMin()
    { return bBoxMin_; }

    const auto& extendedBBoxMax()
    { return bBoxMax_; }

    /*!
     * \brief Calculate the intrinsic permeability
     * \param massFlux The current mass flux leaving the system
     */
    void doUpscaling(const Scalar massFlux)
    {
        // create temporary stringstream with fixed scientifc formatting without affecting std::cout
        std::ostream tmp(std::cout.rdbuf());
        tmp << std::fixed << std::scientific;

        // convert mass to volume flux
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(temperature());
        const Scalar pressureAtOutlet = 1e5 - pressureGradient_ * length_[direction_];
        fluidState.setPressure(0, pressureAtOutlet);
        Scalar density = FluidSystem::density(fluidState, 0);
        Scalar viscosity = FluidSystem::viscosity(fluidState, 0);
        const auto volumeFlux = massFlux / density;

        auto lenght = length_;
        lenght[direction_] = 1.0;
        const auto outflowArea = std::accumulate(lenght.begin(), lenght.end(), 1.0, std::multiplies<Scalar>());
        const auto vDarcy = volumeFlux / outflowArea;
        const auto K = vDarcy / pressureGradient_ * viscosity;
        tmp << dirNames_[direction_] << "-direction";
        tmp << ": Area = " << outflowArea;
        tmp << "; Massflux = " << massFlux << " kg/s";
        tmp << "; v_Darcy = " << vDarcy << " m/s";
        tmp << "; K = " << K << " m^2" << std::endl;
        upscaledData_[direction_] = std::move(UpscaledData{outflowArea, massFlux, vDarcy, K});
    }

    void report(const std::vector<int>& directions) const
    {
        // create temporary stringstream with fixed scientifc formatting without affecting std::cout
        std::ostream tmp(std::cout.rdbuf());
        tmp << std::fixed << std::scientific;

        for (int i : directions)
        {
            tmp << dirNames_[i] << "-direction";
            tmp << ": Area = " << upscaledData_[i].outflowArea << " m^2";
            tmp << "; Massflux = " << upscaledData_[i].massFlux << " kg/s";
            tmp << "; v_Darcy = " << upscaledData_[i].vDarcy << " m/s";
            tmp << "; K = " << upscaledData_[i].K << " m^2" << std::endl;

            // if(isTest_)
            // {
            //     const Scalar actualK = upscaledData_[dimworld-1].K;
            //     const Scalar refK = 6.780611327450674e-14;
            //     if (Dune::FloatCmp::ne<Scalar, Dune::FloatCmp::absolute>(actualK,refK, 1e-20))
            //     {
            //         std::cout << "Upscaled K differs from reference value: " << actualK - refK << std::endl;
            //         std::exit(EXIT_FAILURE);
            //     }
            // }
        }

        const auto& poreVolume = this->gridGeometry().poreVolume();
        const auto totalPoreVolume =  std::accumulate(poreVolume.begin(), poreVolume.end(), 0.0);
        const auto totalSampleVolume = std::accumulate(length_.begin(), length_.end(), 1.0, std::multiplies<Scalar>());
        tmp << "\nTotal pore volume " << totalPoreVolume << " [m^3], total sample volume " << totalSampleVolume << " [m^3], porosity " << totalPoreVolume/totalSampleVolume << std::endl;
        std::cout << "**************************************************\n" << std::endl;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;

        const auto pos = scv.dofPosition();
        if (pos[direction_] < this->gridGeometry().bBoxMin()[direction_] + eps_ || pos[direction_] > this->gridGeometry().bBoxMax()[direction_] - eps_)
        {
            bcTypes.setAllDirichlet();
        }
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
     PrimaryVariables dirichlet(const Element &element,
                                const SubControlVolume &scv) const
     {
         PrimaryVariables values(0.0);

        auto pos = scv.dofPosition();
        if (pos[direction_] < this->gridGeometry().bBoxMin()[direction_] + eps_ )
            values[Indices::pressureIdx] = 1e5;
        else
            values[Indices::pressureIdx] = 1e5 - pressureGradient_ * length_[direction_];

        return values;
    }


    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);

        values[Indices::pressureIdx] = 1e5;
        return values;
    }

    /*!
     * \brief Sets the current direction in which the pressure gradient is applied
     * \param directionIdx The index of the direction (0:x, 1:y, 2:z)
     */
    void setDirection(int directionIdx)
    {
        direction_ = directionIdx;
        this->spatialParams().updateBoundaryFactor(directionIdx);
    }

    // \}

private:
    Scalar eps_;
    int direction_;
    Scalar pressureGradient_;
    std::array<Scalar, dimworld> length_;
    std::array<UpscaledData, dimworld> upscaledData_;
    std::array<std::string, 3> dirNames_;
    bool isTest_;

    //! the bounding box of the whole domain
    GlobalPosition bBoxMin_ = GlobalPosition(std::numeric_limits<Scalar>::max());
    GlobalPosition bBoxMax_ = GlobalPosition(std::numeric_limits<Scalar>::min());
};

} //end namespace

#endif
