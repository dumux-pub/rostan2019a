#openpnm permeability_statoil

import openpnm as op
import scipy as sp
from pathlib import Path


path = Path('Berea/')
#filename_output = 'Berea_MS'

#calculation of L and A for a cube
voxel_size = 3e-6;
voxel_number = 400;
L = voxel_size * voxel_number;
A = L*L;


project = op.io.Statoil.load(path=path, prefix='berea')

pn = project.network

print(pn)

print('Number of pores before trimming: ', pn.Np)
h = pn.check_network_health()
op.topotools.trim(network=pn, pores=h['trim_pores'])
print('Number of pores after trimming: ', pn.Np)

geo = op.geometry.GenericGeometry(network=pn, pores=pn.Ps, throats=pn.Ts)


# add water phase
water = op.phases.Water(network=pn);

mu = 0.001


water['pore.viscosity'] = mu
water['throat.viscosity'] = mu

# Get the average value of the fluid viscosity
mu = sp.mean(water['pore.viscosity'])


# calculate conductance
water.add_model(propname='throat.hydraulic_conductance',
                model=op.models.physics.hydraulic_conductance.valvatne_blunt)

# Specify a pressure difference (in Pa)
delta_P = 100000

flow = op.algorithms.StokesFlow(network=pn, phase=water)
flow.set_value_BC(pores=pn.pores('inlets'), values=100000)
flow.set_value_BC(pores=pn.pores('outlets'), values=0)
flow.run()
# Using the rate method of the stokes flow algorithm
Q = sp.absolute(flow.rate(pores=pn.pores('outlets')))
K = Q*mu*L/(delta_P*A)
print('K(m²)=',K)
print('K(mD)=',K/9.869233e-16)
K2 = flow.calc_effective_permeability(inlets=pn.pores('inlets'), outlets=pn.pores('outlets'), domain_area=A, domain_length=L)
print(K2)

water.update(flow.results())

##change parameters for vtk
#pn['pore.diameter'] = pn['pore.radius']*2
#pn['throat.diameter'] = pn['throat.radius']*2

#op.io.VTK.save(network=pn, phases=water, filename=filename_output)