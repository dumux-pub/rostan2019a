#SNOW algorithm

# import necessary packages
import porespy as ps
import openpnm as op
import nrrd
import scipy as sp
import numpy as np

filename_input = 'Berea.nrrd';
filename_output = 'berea_snow';

# create workspace instance
ws = op.Workspace();    
# clear workspace    
ws.clear();                                  
print(ws.keys()); 
# create project
project = ws.new_project(name=filename_output);
# read inputfile
im, header = nrrd.read(filename_input);
# convert im to bool and invert it
im = sp.array(im, dtype=bool);
im = ~im;
# print porosity
porosity = ps.metrics.porosity(im);
print(porosity);
#application of snow-algorithm
snow_output = ps.networks.snow(im,
                  voxel_size=3e-6,
                  boundary_faces=['top', 'bottom', 'left', 'right', 'front', 'back'],
                  marching_cubes_area=False)
# create empty dummy network
pn = op.network.GenericNetwork(project=project);
# update openpnm pore network
pn.update(snow_output);
# trimming pore network to avoid singularity
print('Number of pores before trimming: ', pn.Np);
h = pn.check_network_health();
op.topotools.trim(network=pn, pores=h['trim_pores']);
print('Number of pores after trimming: ', pn.Np);
# add water phase
water = op.phases.Water(network=pn);
# add shape factors
water.add_model(propname='throat.flow_shape_factors',
               model=op.models.physics.flow_shape_factors.ball_and_stick)


pn['throat.shape_factor'] = ((pn['throat.diameter']/2)**2)/(4*pn['throat.area'])

throats= np.asarray(pn['throat.conns'])
pores_ = np.asarray(pn['pore.all'])
throat_area = np.asarray(pn['throat.area'])
throat_sf = np.asarray(pn['throat.shape_factor'])
numpores = pores_.size
throatsPerPore = [[] for _ in range(numpores)]


for throatIdx, pores in enumerate(throats):
    pore0 = pores[0]
    pore1 = pores[1]

    throatsPerPore[pore0].append(throatIdx)
    throatsPerPore[pore1].append(throatIdx)


throatsPerPore = np.asarray(throatsPerPore)

pore_sf = np.zeros(numpores)

pore_area = np.asarray(pn['pore.area'])


for i in range(numpores):
    throat_area_sum_ = 0
    throatsPerPore_ = np.asarray(throatsPerPore[i], dtype=int)
    throat_area_ = throat_area[throatsPerPore[i]]
    pore_sf_ = 0
    throat_sf_ = throat_sf[throatsPerPore[i]]
    tPP_size = throatsPerPore_.size
    pore_area_ = pore_area[i]

    for j in range(tPP_size):
        if pore_area_ < throat_area_[j]:
            throat_area_sum_ += pore_area_
        else:
            throat_area_sum_ += throat_area_[j]
    for k in range(tPP_size):
        if pore_area_ < throat_area_[j]:
            pore_sf_ += (throat_sf_[k]*pore_area_)/(throat_area_sum_)
        else:
            pore_sf_ += (throat_sf_[k]*throat_area_[k])/(throat_area_sum_)
                
    pore_sf[i] =  pore_sf_
    
pn['pore.shape_factor'] = pore_sf

# check workspace
print(ws.keys()); 
# export to vtk
op.io.VTK.save(network=pn, filename=filename_output)
#op.io.VTK.save(network=pn, phases=water, filename=filename_output)
# save project
ws.save_project(project, filename=filename_output)
# close project)
ws.close_project(project)


