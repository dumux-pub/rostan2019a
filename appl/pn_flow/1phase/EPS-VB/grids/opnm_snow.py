import openpnm as op
import scipy as sp

# create workspace instance
ws = op.Workspace(); 
   
# clear workspace    
ws.clear();   
          
# check workspace                      
print(ws.keys()); 


filename_input = 'berea_snow'
#filename_output = 'Berea_SNOW'

#calculation of L and A for a cube
voxel_size = 3e-6;
voxel_number = 400;
L = voxel_size * voxel_number;
A = L*L;


# create project
ws.load_project(filename_input, overwrite=False)

# check workspace
print(ws.keys()); 

# assign project name
proj_pnm = ws[filename_input]


# assign pore network name
pn = proj_pnm['net_01']


# add water phase
water = op.phases.Water(network=pn);

# viscosity
mu_w = sp.mean(water['pore.viscosity'])


# calculate conductance
water.add_model(propname='throat.hydraulic_conductance',
                model=op.models.physics.hydraulic_conductance.valvatne_blunt)

# Specify a pressure difference (in Pa)
delta_P = 0.94

# flow in x-direction
flow_x = op.algorithms.StokesFlow(network=pn, phase=water)
flow_x.set_value_BC(pores=pn.pores('left'), values=1000.94)
flow_x.set_value_BC(pores=pn.pores('right'), values=1000)
flow_x.run()
# Using the rate method of the StokesFlow algorithm
Qx = sp.absolute(flow_x.rate(pores=pn.pores('left')))
Kx = Qx*mu_w*L/(delta_P*A)
print('Kx(m²)',Kx)
print('Kx(zmD)=',Kx/9.869233e-16)

# flow in y-direction
flow_y = op.algorithms.StokesFlow(network=pn, phase=water)
flow_y.set_value_BC(pores=pn.pores('front'), values=1000.94)
flow_y.set_value_BC(pores=pn.pores('back'), values=1000)
flow_y.run()
# Using the rate method of the StokesFlow algorithm
Qy = sp.absolute(flow_y.rate(pores=pn.pores('front')))
Ky = Qy*mu_w*L/(delta_P*A)
print('Ky(m²)',Ky)
print('Ky(zmD)=',Ky/9.869233e-16)

# flow in z-direction
flow_z = op.algorithms.StokesFlow(network=pn, phase=water)
flow_z.set_value_BC(pores=pn.pores('top'), values=1000.94)
flow_z.set_value_BC(pores=pn.pores('bottom'), values=1000)
flow_z.run()
# Using the rate method of the StokesFlow algorithm
Qz = sp.absolute(flow_z.rate(pores=pn.pores('top')))
Kz = Qz*mu_w*L/(delta_P*A)
print('Kz(m²)',Kz)
print('Kz(zmD)=',Kz/9.869233e-16)


#proj_pnm.export_data(filename=filename_output, filetype='vtk')
