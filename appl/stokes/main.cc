// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem to investigate the permeability of a discretely resolved porous medium.
 */
#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>
#include <dumux/io/grid/gridmanager_alu.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include "problem.hh"

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::StokesPermeability;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run stationary problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto fvGridGeometry = std::make_shared<GridGeometry>(leafGridView);
    fvGridGeometry->update();
    std::cout << "numDofs():" << fvGridGeometry->numDofs() << std::endl;


    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(fvGridGeometry->numCellCenterDofs());
    x[GridGeometry::faceIdx()].resize(fvGridGeometry->numFaceDofs());

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler for the stationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // set up two planes over which fluxes are calculated
    FluxOverSurface<GridVariables,
                    SolutionVector,
                    GetPropType<TypeTag, Properties::ModelTraits>,
                    GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    const Scalar xMin = fvGridGeometry->bBoxMin()[0];
    const Scalar xMax = fvGridGeometry->bBoxMax()[0];
    const Scalar yMin = fvGridGeometry->bBoxMin()[1];
    const Scalar yMax = fvGridGeometry->bBoxMax()[1];
    const Scalar zMin = fvGridGeometry->bBoxMin()[2];
    const Scalar zMax = fvGridGeometry->bBoxMax()[2];

    std::cout << "xMax:" << xMax << std::endl;
    std::cout << "yMax:" << yMax << std::endl;
    std::cout << "zMax:" << zMax << std::endl;
    std::cout << "xMin:" << xMin << std::endl;
    std::cout << "yMin:" << yMin << std::endl;
    std::cout << "zMin:" << zMin << std::endl;

    const auto refinement = getParam<std::size_t>("Grid.Refinement", 0);
    Scalar area = 0.0;

    const std::size_t dirIdx = problem->flowDirection();

    auto addSurface = [&](const std::string& name, const auto& p0, const auto& p1)
    {
        auto axis = std::move(std::bitset<3>{}.set());
        axis.set(dirIdx, false);
        using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar, GridView::dimensionworld-1, GridView::dimensionworld>;
        const auto surface = Rectangle(p0, p1, axis);
        std::cout << "\nsurface " << name << std::endl;
        std::cout << "surface.corner(0) " << surface.corner(0) << std::endl;
        std::cout << "surface.corner(1) " << surface.corner(1) << std::endl;
        std::cout << "surface.corner(2) " << surface.corner(2) << std::endl;
        std::cout << "surface.corner(3) " << surface.corner(3) << std::endl;
        std::cout << std::endl;
        flux.addSurface(name, surface.corner(0), surface.corner(1), surface.corner(2), surface.corner(3));
        area = surface.volume();
    };

    const auto lowerLeftInlet = fvGridGeometry->bBoxMin();
    auto upperRightInlet = fvGridGeometry->bBoxMax();
    upperRightInlet[dirIdx] = lowerLeftInlet[dirIdx];
    addSurface("inlet", lowerLeftInlet, upperRightInlet);

    const Scalar length = fvGridGeometry->bBoxMax()[dirIdx] - fvGridGeometry->bBoxMin()[dirIdx];

    // If we have an odd number of cells, there would not be any cell faces
    // at the postion of the plane (which is required for the flux calculation).
    // In this case, we add half a cell-width to the position in order to make sure that
    // the cell faces lie on the plane. This assumes a regular cartesian grid.
    const Scalar planePosMiddle = fvGridGeometry->bBoxMin()[dirIdx] + 0.5*length;
    std::cout << "planePosMiddle:" << planePosMiddle << std::endl;
    const auto numCells = getParam<std::vector<std::size_t>>("Grid.Cells")[dirIdx] * (1<<refinement);
    const Scalar offset = (numCells % 2 == 0) ? 0.0 : 0.5*(length / numCells);
    std::cout << "offset:" << offset << std::endl;
    auto lowerLeftMiddle = lowerLeftInlet;
    auto upperRightMiddle = upperRightInlet;
    lowerLeftMiddle[dirIdx] += planePosMiddle+offset;
    upperRightMiddle[dirIdx] += planePosMiddle+offset;
    addSurface("middle", lowerLeftMiddle, upperRightMiddle);

    auto lowerLeftOutlet = lowerLeftInlet;
    auto upperRightOutlet = upperRightInlet;
    lowerLeftOutlet[dirIdx] = fvGridGeometry->bBoxMax()[dirIdx];
    upperRightOutlet[dirIdx] = fvGridGeometry->bBoxMax()[dirIdx];
    addSurface("outlet", lowerLeftOutlet, upperRightOutlet);

    // linearize & solve
    Dune::Timer timer;
    nonLinearSolver.solve(x);

    // write vtk output
    vtkWriter.write(1.0);

    // calculate and print mass fluxes over the planes --> mass flux
    flux.calculateMassOrMoleFluxes();

    std::cout << "mass flux at inlet is: " << flux.netFlux("inlet") << std::endl;
    std::cout << "mass flux at middle is: " << flux.netFlux("middle") << std::endl;
    std::cout << "mass flux at outlet is: " << flux.netFlux("outlet") << std::endl;

    const auto rho = getParam<Scalar>("Component.LiquidDensity");
    const auto nu = getParam<Scalar>("Component.LiquidKinematicViscosity");
    const auto deltaP = getParam<Scalar>("Problem.DeltaP");

    const auto volumeFlux = flux.netFlux("outlet") / rho;
    const auto vDarcy = volumeFlux / area;
    const auto K = vDarcy / deltaP * nu;
    std::cout << "Density:" << rho;
    std::cout << "; Viscosity:" << nu;
    std::cout << "; Volume Flux:" << volumeFlux;
    std::cout << "; length L:" << length;
    std::cout << "; Outflowarea A:" << area;
    std::cout << "; v_Darcy = " << vDarcy << " m/s";
    std::cout << "; K = " << K << " m^2" << std::endl;

    timer.stop();

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";


    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
