dune_symlink_to_source_files(FILES 3d_x 3d_y 3d_z)

dune_add_test(NAME 3d_stokes
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK)
