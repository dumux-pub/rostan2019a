/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the cake grid manager
 */
#include <config.h>
#include <iostream>
#include <cmath>
#include <string>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/gmshwriter.hh>

#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/common/parameters.hh>


/*!
 * \brief A method providing an () operator in order to select elements for a subgrid.
 */
typedef unsigned char BYTE;
// Reads the raw file into an array
template<typename Container>
Container readRawFileToContainer(const std::string& filename)
{
    // open the file:
    std::streampos fileSize;
    std::ifstream file(filename, std::ios::binary);

    // get its size:
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<BYTE> fileData(fileSize);
    file.read((char*) &fileData[0], fileSize);
    std::cout << "size of array = " << fileData.size() << std::endl;
    return fileData;
}


int main(int argc, char** argv) try
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);

    constexpr int dim = 3;

    {
        Dune::Timer timer;
        std::cout << "Constructing SubGrid from raw" << std::endl;
        
        using HostGrid = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<double, dim> >;
        using SubGrid = Dune::SubGrid<dim, HostGrid>;

        using HostGridManager = Dumux::GridManager<HostGrid>;

        HostGridManager hostGridManager;

        
        hostGridManager.init();
        auto& hostGrid = hostGridManager.grid();


        const auto& gridView = hostGrid.leafGridView();
        std::string geomFile = getParam<std::string>("Grid.GeometryFile");
        auto v = readRawFileToContainer<std::vector<unsigned char>>(geomFile);
        std::cout << typeid(v).name() << std::endl;


        auto elementSelector = [&gridView, &v](const auto& element)
        {
          auto index = gridView.indexSet().index(element);
          //std::cout << "index = " << index << std::end;
          return v[index] == 0;
        }; 


        Dumux::GridManager<SubGrid> subgridManager;
        subgridManager.init(hostGrid, elementSelector);


        Dune::VTKWriter<typename  SubGrid::LeafGridView> vtkWriter(subgridManager.grid().leafGridView());
        vtkWriter.write("stokes_grid_rtm_");

        Dune::GmshWriter<typename  SubGrid::LeafGridView> gmshWriter(subgridManager.grid().leafGridView());
        gmshWriter.write("stokes_grid_msh.msh");

        std::cout << "Constructing took "  << timer.elapsed() << " seconds.\n";
   
    
    }

    return 0;
}
///////////////////////////////////////
//////// Error handler ////////////////
///////////////////////////////////////
catch (const Dumux::ParameterException& e) {
    std::cerr << e << ". Abort!\n";
    return 1;
}
catch (const Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 3;
}
catch (...) {
    std::cerr << "Unknown exception thrown!\n";
    return 4;
}
