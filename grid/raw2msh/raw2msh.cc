/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the cake grid manager
 */
#include <config.h>
#include <iostream>
#include <cmath>
#include <string>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/gmshwriter.hh>
//#include <dune/grid/io/file/dgfparser.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/common/parameters.hh>


/*!
 * \brief A method providing an () operator in order to select elements for a subgrid.
 */
typedef unsigned char BYTE;
// Reads the raw file into an array
template<typename Container>
Container readRawFileToContainer(const std::string& filename)
{
    // open the file:
    std::streampos fileSize;
    std::ifstream file(filename, std::ios::binary);

    // get its size:
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<BYTE> fileData(fileSize);
    file.read((char*) &fileData[0], fileSize);
    std::cout << "size of array = " << fileData.size() << std::endl;
    return fileData;
}

// getConnectedElements(gridView, v)

// alles anpassen und richtig rum machen
template<class GridView>
//std::vector<bool> getConnectedElements(const GridView& gridView, const std::vector<BYTE>& v)
std::vector<bool> getConnectedElements(const GridView& gridView, const std::vector<unsigned char>& v, std::vector<double>& size)
{    
     std::vector<bool> elementIsConnected(gridView.size(0), false);
     for (const auto& element : elements(gridView))
        {
            const auto eIdx = gridView.indexSet().index(element);
            if (elementIsConnected[eIdx])
                continue;

            //if (v[eIdx] !=0)
            //std::cout << "v[" << eIdx << "]" << v[eIdx] << std::endl;
            //std::cout << "Richtig" << std::endl;
            if (v[eIdx] !=0)
               continue;

            // try to find a seed from which to start the search process
            bool isSeed = false;
            //if (v[eIdx] == 0)
            if (v[eIdx] == 0)
            {
               for (const auto& intersection : intersections(gridView, element))
               {    // hier eventuell tauschen
                    //if (intersection.geometry().center()[0] < 1e-8)
                    //std::cout << "Intersection:" << intersection.geometry().center()[0] << std::endl;
                    // x-direction
                    if (intersection.geometry().center()[0] < 1e-8 || intersection.geometry().center()[0] > size[0]-1e-8)
                    {
                         isSeed = true;
                         //std::cout << "SEED_true" << std::endl;
                         break;
                    }
                    // y-direction
                    /*
                    if (intersection.geometry().center()[1] < 1e-8 || intersection.geometry().center()[1] > size[1]-1e-8)
                    {
                         isSeed = true;
                         //std::cout << "SEED_true" << std::endl;
                         break;
                    }
                    // z-direction 
                    if (intersection.geometry().center()[2] < 1e-8 || intersection.geometry().center()[2] > size[2]-1e-8)
                    {
                         isSeed = true;
                         //std::cout << "SEED_true" << std::endl;
                         break;
                    }
                    */
               }
            }

            
            if (isSeed)
            {
                elementIsConnected[eIdx] = true;

                // use iteration instead of recursion here because the recursion can get too deep
                using Element = std::decay_t<decltype(element)>;
                std::stack<Element> elementStack;
                elementStack.push(element);
                while (!elementStack.empty())
                {
                    auto e = elementStack.top();
                    elementStack.pop();
                    for (const auto& intersection : intersections(gridView, e))
                    {
                        if (!intersection.boundary())
                        {
                            auto outside = intersection.outside();
                            auto nIdx = gridView.indexSet().index(outside);
                            if (!elementIsConnected[nIdx] && v[nIdx] == 0)
                            {
                                elementIsConnected[nIdx] = true;
                                elementStack.push(outside);
                            }
                        }
                    }
                }
            }
        }

     return elementIsConnected;
}


int main(int argc, char** argv) try
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);

    constexpr int dim = 3;

    {
        Dune::Timer timer;
        std::cout << "Constructing SubGrid from raw" << std::endl;
        
        using HostGrid = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<double, dim> >;
        using SubGrid = Dune::SubGrid<dim, HostGrid>;

        using HostGridManager = Dumux::GridManager<HostGrid>;

        HostGridManager hostGridManager;

        
        hostGridManager.init();
        auto& hostGrid = hostGridManager.grid();


        const auto& gridView = hostGrid.leafGridView();
        std::string geomFile = getParam<std::string>("Grid.GeometryFile");
        std::vector<double> size = getParam<std::vector<double>>("Grid.UpperRight");
        
        // Test
        std::string exportName = getParam<std::string>("Name.ExportName");

        //Container readRawFileToContainer(const std::string& filename)
        auto v = readRawFileToContainer<std::vector<unsigned char>>(geomFile);
        

        //const std::vector<bool> connectedElements = getConnectedElements(gridView, v);
        auto connectedElements = getConnectedElements(gridView, v, size);
        //const auto connectedElements = getConnectedElements<std::vector<bool>>(gridView, v);

       
        auto elementSelector = [&gridView, &v, &connectedElements](const auto& element)
        {
          auto index = gridView.indexSet().index(element);
          //std::cout << "index = " << index << std::end;
          //return v[index] == 0;
          //return connectedElements[eIdx];
          return connectedElements[index];
        }; 

                  
        Dumux::GridManager<SubGrid> subgridManager;
        subgridManager.init(hostGrid, elementSelector);

        Dune::VTKWriter<typename  SubGrid::LeafGridView> vtkWriter(subgridManager.grid().leafGridView());
        vtkWriter.write(exportName);

        Dune::GmshWriter<typename  SubGrid::LeafGridView> gmshWriter(subgridManager.grid().leafGridView());
        gmshWriter.write(exportName + ".msh");

        Dune::DGFWriter<typename  SubGrid::LeafGridView> dgfWriter(subgridManager.grid().leafGridView());
        dgfWriter.write(exportName + ".dgf");

        std::cout << "Constructing took "  << timer.elapsed() << " seconds.\n";
   
    
    }

    return 0;
}
///////////////////////////////////////
//////// Error handler ////////////////
///////////////////////////////////////
catch (const Dumux::ParameterException& e) {
    std::cerr << e << ". Abort!\n";
    return 1;
}
catch (const Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 3;
}
catch (...) {
    std::cerr << "Unknown exception thrown!\n";
    return 4;
}
